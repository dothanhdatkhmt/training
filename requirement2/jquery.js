$.fn.inner_float = function(obj) {
	var child_id = $(this).attr("id");
	var parent_height = parseInt($(this).parent().height());
	var child_height = parseInt($(this).height());
	var parent_top = parseInt($(this).parent().offset().top);
  
  var top = parseInt(obj.top);
  var margin_top = parseInt(obj.margin_top);
  var margin_bottom = parseInt(obj.margin_bottom);
  
 	init();
  
  var target_one = parent_top + margin_top - top;
  var target_two = parent_top + parent_height - margin_bottom - child_height - top;
	
  $(window).bind( "scroll", function() {
    scroll();
  });

  
  function init(){
  		$("#"+child_id).parent().css("position","relative");
      
      $("#"+child_id).css("position","absolute").css("bottom","").css("top",margin_top);
  }
  
	function scroll() {
		var scrollTop=document.documentElement.scrollTop;
    
    if( scrollTop <= target_one ){
    		$("#"+child_id).css("position","absolute").css("bottom","").css("top",margin_top);
    }else if( scrollTop <= target_two ){
    		var parent_width = parseInt($("#"+child_id).parent().width());
    				$("#"+child_id).css("position","fixed").css("bottom","").css("top",top).css('width', parent_width);
    } else{
    $("#"+child_id).css("position","absolute").css("bottom",margin_bottom).css("top","");
    }
	}
}
